FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flask_restful

COPY python-api.py /opt/api/python-api.py
CMD ["python3", "/opt/api/python-api.py"]
